//
//  ViewController.swift
//  The Night Porter
//
//  Created by Elias Júnior Schmeisk Pinheiro on 21/11/16.
//  Copyright © 2016 Elias Júnior Schmeisk Pinheiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let testeA = ["teste1","teste2", "teste3"]
    let testeB = ["sad","sdf", "sda"]
    let testeC = ["qwe","ert", "yuui"]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return testeA.count
        case 1:
            return testeB.count
        default:
            return testeC.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        var task: String
        switch indexPath.section {
        case 0:
            task = testeA[indexPath.row]
        case 1:
            task = testeB[indexPath.row]
        default:
            task = testeC[indexPath.row]
        }
        cell.textLabel!.text = task
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "TesteA"
        case 1:
            return "TesteB"
        default:
            return "testeC"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You select \(indexPath.row)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

